Yii2 Dynamic Setting
=================

## Description

Dynamic setting management with database table.

## Installation

```
composer require buibr/yii2-dynamic-settings

yii migrate --migrationPath=vendor/buibr/yii2-dynamic-settings/migrations/
```

## Usage

Extend settings with new clase.

```php
class Crm extends \buibr\yii2settings\Settings
{
    /**
     * Set the group of settings.
     */
    public static function group()
    {
        return "crm";
    }

    /**
     * The default setting for begining on new installation.
     */
    public static function defaults()
    {
        return [
            [
                'name'      =>'default_vat',
                'value'     => 0,  
                'text'      => 'What is the autoselected VAT ',
                'type'      => self::TYPE_SELECT,
                'source'    => self::SOURCE_CLASS,
                'source_data' => '\app\models\VatGroups'
            ],
            [
                'name'      => 'invoicing_date',
                'value'     => 3,  
                'text'      => 'In which day of the month we will generate invoices for clients.',
                'type'      => self::TYPE_TEXT,
            ],
        ];
    }
}
```

call like:

```php

foreach(Crm::all() as $sett) {
    $sett->render();
}

```