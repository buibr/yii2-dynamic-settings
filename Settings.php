<?php

namespace buibr\yii2settings;

use Yii;
use yii\helpers\Html;
use yii\db\ActiveQuery;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "settings".
 *
 * @property int $mid_id
 * @property string $name
 * @property string $value
 * @property string $text
 * @property string $type = (text, select, checkbox, raw)
 * @property string $source = class,array,json,json_url,csv,csv_url;
 * @property string $source_data = class: \app\models\crm\Discounts | json: [{"1":"Yes", "0":"No"}] | csv: Yes,No
 */
class Settings extends \yii\db\ActiveRecord
{

    /**
     *  Render type.
     */
    const TYPE_TEXT     = 'text';
    const TYPE_SELECT   = 'select';
    const TYPE_BOLEAN   = 'boolean';
    const TYPE_RAW      = 'raw';

    /**
     *  Source type.
     */
    const SOURCE_CLASS  = 'class';
    const SOURCE_ARRAY  = 'array';
    const SOURCE_JSON   = 'json';
    const SOURCE_JSONU  = 'json_url';
    const SOURCE_CSV    = 'csv';
    const SOURCE_CSVU   = 'csv_url';

    /**
     * Will be saved her all.
     */
    protected static $loaded;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%settings}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['group', 'name',], 'required'],
            [['text', 'type', 'source', 'source_data'], 'string'],
            [['group', 'name'], 'string', 'max' => 45],
            [['value'], 'string', 'max' => 255],
            [['name', 'group'], 'unique', 'targetAttribute' => ['name', 'group']],
        ];
    }

    /**
     *  Defualt list for all categories.
     */
    public static function group()
    {
        return null;
    }

    /**
     *  default of all
     */
    public static function defaults()
    {
        return [];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'Setting ID',
            'group' => 'Group',
            'name' => 'Name',
            'value' => 'Value',
            'text' => 'Text',
            'type' => 'Type',
            'source' => 'Source',
            'source_data' => 'Source Data',
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function afterFind()
    {
        self::$loaded[$this->group][$this->name] = $this;
    }

    /**
     *  Get all settings from db at once
     */
    public static function fetch()
    {
        if(empty(self::$loaded))
            return self::find()->all();
        return self::$loaded;
    }

    /**
     * Refresh data from db all.
     */
    public static function fresh()
    {
        self::$loaded = null;
        return self::find()->all();
    }

    /**
     * Return all settings/crm keys.
     */
    public static function all( $class=null )
    {
        self::fresh();

        $class = $class?:get_called_class();
        $group = $class::group();

        if($class::group() === null)
        {
            return self::$loaded;
        }
        else
        {
            $arr = [];
            foreach($class::defaults() as $k){
                $arr[$k['name']] = new $class($k);
            }

            $loaded = self::$loaded[$group];
            if(empty($loaded)){
                $loaded = [];
            }

            return \array_merge( $arr, $loaded);
        }
    }

    /**
     * Returns all from db if null then returns default values.
     */
    public static function allOrDefault()
    {
        self::fetch();

        if(empty(self::$loaded[get_called_class()::group()]))
            return \get_called_class()::defaults();
        else
            return self::$loaded[ \get_called_class()::group() ];
    }

    /**
     * Return all or default Associaative array.
     */
    public static function allOrDefaultAssoc()
    {
        self::fresh();

        if(empty(self::$loaded[get_called_class()::group()]))
            return ArrayHelper::map(get_called_class()::defaults(), 'name', 'value');
        else
            return ArrayHelper::map(self::$loaded[get_called_class()::group()], 'name', 'value');
    }

    /**
     * Return all from db and check if any from default is not in list.
     * @return array Settings
     */
    public static function allWithDefault()
    {
        self::fresh();

        $class = get_called_class();

        if(empty(self::$loaded[$class::group()])) {
            foreach($class::defaults() as $v){
                if(!isset( self::$loaded[$class::group()][$v['name']]) ) {
                    self::$loaded[$class::group()][$v['name']] = new $class($v);
                }
            }
        }
        else {  // if in any condition we add new setting on default in class ..
            $arr = $class::defaults();
            foreach($arr as $v){
                if(!isset( self::$loaded[$class::group()][$v['name']]) ) {
                    self::$loaded[$class::group()][$v['name']] = new $class($v);
                }
            }
        }

        return self::$loaded[$class::group()];
    }

    /**
     * Return the object of Settings active record,
     * 
     * @param string $key 
     *  - the key of the configuration
     * @param bool $throw
     *  - Throw ErrorException or not 
     * @throws \ErrorException
     * @return object Settings;
     */
    public static function getObject( string $key, bool $throw = false )
    {
        self::fetch();

        if(isset(self::$loaded[get_called_class()::group()][$key]))
            return self::$loaded[get_called_class()::group()][$key];
        elseif( ($object = self::getDefaultsObject($key)) !== null )
            return $object;
        elseif($throw)
            throw new \ErrorException("Configuration setting is not found.");
        else
            return null;
    }

    /**
     * Return an object from defaults of the child class.
     * 
     * @param string $key 
     *  - the key of the configuration
     * @param bool $throw
     *  - Throw ErrorException or not 
     * @throws \ErrorException
     * @return object Settings;
     */
    public static function getDefaultsObject( string $key, bool $throw = false, $class= null)
    {

        $class = $class ?: \get_called_class();

        if( !empty($loaded = get_called_class()::defaults()) ){

            foreach($loaded as $object){

                if($key === $object['name']){
                    $object = new $class($object);
                    $object->group = $class::group();
                    return $object;
                }

            }

            if($throw) {
                throw new \ErrorException("Configuration setting is not found.");
            }
            else {
                return null;
            }

        }
        elseif($throw) {
            throw new \ErrorException("Configuration setting is not found.");
        }
        else {
            return null;
        }
    }

    /**
     * Return the value of an dynamic configuration from database
     * 
     * @param string $key 
     *  - the key of the configuration
     * @param bool $throw
     *  - Throw ErrorException or not 
     * @throws \ErrorException
     * @return null;
     */
    public static function get( string $key, bool $throw = false )
    {
        self::fetch();

        if(isset(self::$loaded[get_called_class()::group()][$key]))
            return self::$loaded[get_called_class()::group()][$key]->value;
        elseif($throw)
            throw new \ErrorException("Configuration setting is not found.");
        else
            return null;
    }

    /**
     * Create or update settings
     * @param string $key 
     *  - the key of the configuration
     * @param mixed $val
     *  - the value of the config
     * @param string $text
     *  - This is more to explain for what is this settings. Can be used on setting nested params
     */
    public static function set( string $key, $val, string $type = "text", string $source = null, string $source_data = null,   string $text=null )
    {
        $obj = self::getObject($key);

        if($obj === null){
            $obj = new self;
            $obj->group = get_called_class()::group();
        }

        $obj->name          = isset($key) ?  $key : null;
        $obj->value         = isset($val) ?  $val : null;
        $obj->text          = isset($text) ?  $text : null;
        $obj->type          = isset($type) ?  $type : null;
        $obj->source        = isset($source) ?  $source : null;
        $obj->source_data   = isset($source_data) ?  $source_data : null;
        $obj->save();

        return self::fresh();
    }

    /**
     * Check if an setting is enabled.
     * This means that if an key exists in db and 
     * @param string $key 
     *  - the key of the configuration
     * @param mixed $val
     *  - the value of the config
     * @param string $text
     *  - This is more to explain for what is this settings. Can be used on setting nested params
     */
    public static function en( string $key, callback $onTrue = null, callback $onFalse = null )
    {
        //  get key from
        $obj = self::getObject($key);

        //  if not from db then ceck from defaults of child class.
        if($obj === null){
            $obj = self::getDefaultsObject($key);
        }

        if(empty($obj->value)):
            if(\is_callable($onFalse)){
                $onFalse();
            }
            else {
                return false;
            }
        else:
            if(\is_callable($onTrue)){
                $onTrue();
            }
            else {
                return true;
            }
        endif;
    }

    /**
     * Check if an setting is enabled.
     * This means that if an key exists in db and 
     * @param string $key 
     *  - the key of the configuration
     * @param mixed $val
     *  - the value of the config
     * @param string $text
     *  - This is more to explain for what is this settings. Can be used on setting nested params
     */
    public static function is( string $key, $onTrue = null, $onFalse=null)
    {
        //  get key from
        $obj = self::getObject($key);

        //  if not from db then ceck from defaults of child class.
        if($obj === null) {
            $obj = self::getDefaultsObject($key);
        }

        if(empty($obj->value)):
            if(\is_callable($onFalse)){
                $onFalse();
            }
            else {
                return false;
            }
        else:
            if(\is_callable($onTrue)){
                $onTrue();
            }
            else {
                return true;
            }
        endif;
    }

    /**
     *  Render one option based on the parameters and type of setting.
     * 
     * @return string mixed
     */
    public function render( array $opt =[])
    {
        $data = $this->sourceResolver();

        switch( $this->type )
        {
            case self::TYPE_SELECT:
                $content =  Html::dropDownList("{$this->formName()}[{$this->name}]", $this->value, $data , $opt);
            break;
            case self::TYPE_BOLEAN:
                $content =  Html::dropDownList("{$this->formName()}[{$this->name}]", $this->value, ['1'=>'Yes', '0'=>'No'], $opt);
            break;
            case self::TYPE_TEXT:
                $content =  Html::input('text', "{$this->formName()}[{$this->name}]", $this->value, $opt);
            break;
            case self::TYPE_RAW:
                $content =  Html::tag('textarea', $this->value, \array_merge(['name'=>"{$this->formName()}[{$this->name}]", 'rows'=>5], $opt) );
            break;

            default:
                $content =  Html::input('text', "{$this->formName()}[{$this->name}]", $this->value, $opt);
                break;
        }

        return $content;
    }


    /**
     * 
     */
    private function sourceResolver( )
    {
        switch ($this->source) {
            
            case self::SOURCE_CLASS:
            
                $class = $this->source_data;
                if(!class_exists( $class )):
                    return null;
                endif;

                $primary    = $class::primaryKey()[0];
                $display    = $this->defineRenderAttribute( $class );
                
                $data       = $class::find()->all();
                if(empty($data)) {
                    return [];
                }
                
                return ArrayHelper::map($data, $primary, $display);

            break;

            case self::SOURCE_ARRAY:

                $data = unserialize( $this->source_data );

                return $data;

            break;

            case self::SOURCE_JSON :
                
                $data = \json_decode( $this->source_data, true);
                return $data;

            break;

            case self::SOURCE_JSONU :

                $content    = \file_get_contents( $this->source_data );
                $data       = \json_decode( $content, true );

                return $data;

            break;

            case self::SOURCE_CSV :

            break;

            case self::SOURCE_CSVU :

            break;

            default:
                return null;
                break;
        }
    }


    /**
     * 
     */
    private function defineRenderAttribute( $cls )
    {
        if(!class_exists($cls)):
            return  null;
        endif;

        $class = new $cls;

        foreach ($class::getTableSchema()->getColumnNames() as $name):
            if (!strcasecmp($name, 'name') || !strcasecmp($name, 'title')) {
                return $name;
            }
        endforeach;

        $pk = $class::primaryKey();

        return $pk[0];
    }


    /**
     * Label to camels case revert
     */
    public function label()
    {
        return ucWords(str_replace(['-', '_'], ' ', $this->name));
    }

}
