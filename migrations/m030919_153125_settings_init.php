<?php
use yii\base\InvalidConfigException;
use yii\caching\DbCache;
use yii\db\Migration;

/**
 * Initializes Settings tables.
 *
 * @author Burhan Ibraimi <burhan@wflux.pro>
 */
class m030919_153125_settings_init extends Migration
{

    /**
     *  Table name
     */
    private $newTable = '{{%settings}}';


    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable($this->newTable, [
            'id' => $this->primaryKey(),
            'group' => $this->string(100),
            'name' => $this->string(100),
            'value' => $this->string(255),
            'text' => $this->text(),
            'type' => "ENUM('text', 'select', 'boolean','') NOT NULL DEFAULT '' ",
            'source' => "ENUM('class','array','json','json_url','csv','csv_url','') NOT NULL DEFAULT '' ",
            'source_data'   => $this->text(),
        ], $tableOptions);

        $this->createIndex('idx_buibr_settgroup', $this->newTable, 'group');
        $this->createIndex('idx_buibr_settname', $this->newTable, 'name');
        $this->createIndex('idx_buibr_settvalue', $this->newTable, 'value');
        
    }

    public function down()
    {
        $this->dropIndex($this->newTable, 'idx_buibr_settgroup');
        $this->dropIndex($this->newTable, 'idx_buibr_settname');
        $this->dropIndex($this->newTable, 'idx_buibr_settvalue');
        $this->dropTable($this->newTable);
    }
}